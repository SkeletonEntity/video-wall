(concatenate-manifests
 (list (specifications->manifest
	(list "ghc@9.2"
	      "cabal-install"
	      "ghc-hpack"))
       (package->development-manifest
	(specification->package "cabal-install"))))
