{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Main where

import Control.Conditional
import Control.Lens.Operators
import Control.Monad.Catch
import Control.Monad.Cont (liftIO)
import Data.Either.Combinators
import qualified Data.List.NonEmpty as NE
import Data.List.Split
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Data.Text.Internal.Builder
  ( fromString,
    fromText,
  )
import Data.Time
import Katip
import Katip.Core (intercalateNs)
import Katip.Scribes.Handle
import Language.Haskell.TH
import qualified Shelly as S
import System.Console.GetOpt
import System.Environment
import System.FilePath
import System.IO
import System.Random
import Text.Printf
import Text.Read
import VideoWall.App
import VideoWall.Desktop
import VideoWall.Screen
import VideoWall.Util
import VideoWall.XWinWrap

default (T.Text)

main :: IO ()
main = do
  args <- liftIO getArgs
  (eith, files) <- liftIO $ videoWallOpts args
  case eith of
    Left err -> TIO.putStrLn err
    Right opts -> start args files opts

start args files opts = do
  handleScribe <-
    mkHandleScribeWithFormatter
      myFormat
      ColorIfTerminal
      stdout
      (permitItem (opts ^. logSeverity))
      V2
  let makeLogEnv =
        registerScribe "stdout" handleScribe defaultScribeSettings
          =<< initLogEnv "video-wall" "production"
  bracket makeLogEnv closeScribes $ \le -> do
    let env = LogState mempty mempty le
    runLogger env $ do
      $(logTM) InfoS "video-wall started"
      $(logTM) InfoS $ ls $ "args = " <> show args
      files <- liftIO $ allFiles' (opts ^. recursiveSearch) args
      (opts, files) <- do
        load <- if null files then liftIO loadOptions else pure Nothing
        when (isJust load) ($(logTM) InfoS "loaded previous options")
        when
          (isNothing load && null files)
          (liftIO $ fail "No arguments specified and no options saved")
        pure $ fromMaybe (opts, files) load
      $(logTM) InfoS $ ls $ "found files:\n" <> show files
      when (null files) (liftIO $ fail "No files found")
      when
        (opts ^. save)
        ( do
            $(logTM) InfoS "saving set options"
            liftIO $ saveOptions (opts, files)
        )
      filesNE <-
        if opts ^. pickOne
          then do
            n <- fst . uniformR (0, length files - 1) <$> initStdGen
            $(logTM) InfoS $ ls' $ printf "randomly picked %s" (files !! n)
            pure $ NE.singleton $ files !! n
          else pure $ fromJust . NE.nonEmpty $ files
      screen <-
        flip
          fromMaybe
          (pure <$> opts ^. geometry)
          <$> liftSh
          $ S.silently
          $ getScreen (opts ^. displayNumber)

      $(logTM) InfoS $
        ls' $
          printf
            "found display geometry: %s"
            (show screen)
      let xwin =
            XWinWrap screen (opts ^. xWinWrapArgs) (opts ^. mpvArgs) filesNE (opts ^. fitType)
      case opts ^. videoBehavior of
        Loop -> runLoop xwin
        LoopRandom -> runLoopRandom xwin
        SwitchEvery n -> runSwitchEvery n (opts ^. forceSwitch) xwin
        SwitchEveryRandom n ->
          runSwitchEveryRandom n (opts ^. forceSwitch) xwin
      $(logTM) InfoS "video-wall exited"

myTimeFormat :: UTCTime -> T.Text
myTimeFormat = T.pack . formatTime defaultTimeLocale "%Y-%m-%d %H:%M:%S"

myFormat :: LogItem a => ItemFormatter a
myFormat withColor verb Item {..} =
  brackets nowStr
    <> brackets (mconcat $ map fromText $ intercalateNs _itemNamespace)
    <> brackets (fromText (renderSeverity' _itemSeverity))
    <> mconcat ks
    <> maybe mempty (brackets . fromString . myLocationToString) _itemLoc
    <> fromText " "
    <> unLogStr _itemMessage
  where
    nowStr = fromText (myTimeFormat _itemTime)
    ks = map brackets $ getKeys verb _itemPayload
    renderSeverity' severity =
      colorBySeverity withColor severity (renderSeverity severity)

myLocationToString :: Loc -> String
myLocationToString loc = loc_filename loc ++ ':' : line loc ++ ':' : char loc
  where
    line = show . fst . loc_start
    char = show . snd . loc_start

videoWallOpts :: [String] -> IO (Either T.Text Options, [String])
videoWallOpts argv = case getOpt Permute options argv of
  (o, n, []) -> pure (foldl (flip id) (Right defaultOptions) o, n)
  (_, _, err) -> ioError (userError (concat err <> usageInfo header options))

header = "Usage: video-wall [OPTIONS] args.."

options :: [OptDescr (Either T.Text Options -> Either T.Text Options)]
options =
  [ Option
      ['v']
      ["verbosity"]
      (ReqArg parseVerbosity "VERBOSITY")
      "An integer [0,4] or debug, info, warn, error, none",
    Option
      ['d']
      ["display"]
      (ReqArg parseDisplayNumber "INTEGER")
      ( "A positive integer denoting which display's desktop to replace. "
          <> "Displays are ordered by where they appear in xrandr, except 0 is "
          <> "always the primary display"
      ),
    Option
      ['g']
      ["geometry"]
      (ReqArg parseGeometry "GEOMETRY")
      ( "The exact geometry to display the video in. This is the XWinWrap -g option. "
          <> "Formatted as WxH or WxH+X+Y"
      ),
    Option
      ['x']
      ["dimensions"]
      (ReqArg parseDisplayDimensions "DIMENSIONS")
      ( "The pixel dimensions to scale MPV to, for when scale, stretch, and crop "
          <> "do not work. Formatted as WxH"
      ),
    Option
      ['a']
      ["args", "xwinwragargs"]
      (ReqArg parseArgs "ARGS")
      "The args to pass to xWinWrap. Do not pass -g.",
    Option
      ['m']
      ["mpv", "mpvArgs"]
      (ReqArg parseMPVArgs "ARGS")
      ( "The args to pass to MPV. Do not pass --wid, --video-scale-x, "
          <> "or --video-scale-y"
      ),
    Option ['R'] ["recursive"] (NoArg setRecursive) "Search folders recursively",
    Option
      ['l']
      ["loop"]
      (NoArg setLoop)
      "Loop videos in the playlist. This is the default behavior",
    Option ['L'] ["loopRandom"] (NoArg setLoopRandom) "Randomizes the playlist",
    Option
      ['s']
      ["switch"]
      (ReqArg parseSwitch "MILLIS")
      ( "Number of milliseconds minimum to spend on a video before switching "
          <> "to the next in the playlist. MILLIS clamped to [1000,inf)"
      ),
    Option
      ['S']
      ["switchRandom"]
      (ReqArg parseSwitchRandom "MILLIS")
      ( "Number of milliseconds minimum to spend on a video before switching "
          <> "to the next in a randomized playlist. MILLIS clamped to [1000,inf)"
      ),
    Option
      ['f']
      ["fit"]
      (ReqArg parseFitType "FIT")
      ( "The method to use for fitting the video to the desktop. Can be "
          <> "crop, scale, stretch. This does not effect a loop (-l -L) with "
          <> "more than one file in the playlist"
      ),
    Option
      ['F']
      ["forceSwitch"]
      (NoArg setForceSwitch)
      "When using switch (-s, -S) do not wait for video to end before switching",
    Option
      ['p']
      ["pickOne"]
      (NoArg setPickOne)
      "When there are multiple files, pick one randomly",
    Option ['n'] ["noSave"] (NoArg setNoSave) "Disable saving these options"
  ]
  where
    parseVerbosity str optsE = do
      sev <- parseSeverity str
      optsE <&> logSeverity .~ sev
      where
        parseSeverity str
          | str `elem` ["4", "debug"] = Right DebugS
          | str `elem` ["3", "info"] = Right InfoS
          | str `elem` ["2", "warning", "warn"] = Right WarningS
          | str `elem` ["1", "error"] = Right ErrorS
          | str `elem` ["0", "emergency", "none"] = Right EmergencyS
          | str == "notice" = Right NoticeS
          | str == "critical" = Right CriticalS
          | str == "alert" = Right AlertS
          | otherwise = Left ("Unknown verbosity: " <> T.pack str)

    parseDisplayNumber str optsE = do
      d <- maybeToRight "Display number must be a whole number" (readMaybe str)
      if d < 0
        then Left "Display number must be positive"
        else optsE <&> displayNumber .~ d

    parseGeometry str optsE = do
      g <- case splitWhen (`elem` ['x', '+']) str of
        [w, h, x, y] ->
          maybeToRight msg $ Rectangle <$> readMaybe w <*> readMaybe h <*> readMaybe x <*> readMaybe y
        [w, h] ->
          maybeToRight msg $ Rectangle <$> readMaybe w <*> readMaybe h <*> pure 0 <*> pure 0
      optsE <&> geometry ?~ g
      where
        msg = "Could not read geometry, write as WxH or WxH+X+Y"

    parseDisplayDimensions str optsE = case splitWhen (== 'x') str of
      [x, y] -> do
        xy <- maybeToRight msg ((,) <$> readMaybe x <*> readMaybe y)
        optsE <&> fitType .~ Set xy
      _ -> Left msg
      where
        msg = "Could not read dimensions, write as WxH"

    parseMPVArgs str optsE = optsE <&> mpvArgs .~ words str

    parseArgs str optsE = optsE <&> xWinWrapArgs .~ words str

    setRecursive optsE = optsE <&> recursiveSearch .~ True

    setLoop optsE = optsE <&> videoBehavior .~ Loop

    setLoopRandom optsE = optsE <&> videoBehavior .~ LoopRandom

    parseSwitch str optsE = do
      n <- maybeToRight "Millis must be a whole number" (readMaybe str)
      optsE <&> videoBehavior .~ SwitchEvery (max 1000 n)

    parseSwitchRandom str optsE = do
      n <- maybeToRight "Millis must be a whole number" (readMaybe str)
      optsE <&> videoBehavior .~ SwitchEveryRandom (max 1000 n)

    parseFitType str optsE = do
      fit <- parse str
      optsE <&> fitType .~ fit
      where
        parse str
          | str == "crop" = Right Crop
          | str == "scale" = Right Scale
          | str == "stretch" = Right Stretch
          | otherwise = Left ("Unknown fit type: " <> T.pack str)

    setForceSwitch optsE = optsE <&> forceSwitch .~ True

    setPickOne optsE = optsE <&> pickOne .~ True

    setNoSave optsE = optsE <&> save .~ False
