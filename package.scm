(define-module (themescheme)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build utils)
  #:use-module (guix build haskell-build-system)
  #:use-module (guix build-system haskell)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages haskell)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages haskell-web)
  #:use-module (gnu packages haskell-check)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages video)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xorg))

(define-public ghc-string-conv
  (package
    (name "ghc-string-conv")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (hackage-uri "string-conv" version))
              (sha256
               (base32
                "15lh7b3jhhv4bwgsswmq447nz4l97gi0hh8ws9njpidi1q0s7kir"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "string-conv")))
    (native-inputs (list ghc-quickcheck-instances ghc-tasty
                         ghc-tasty-quickcheck))
    (home-page "https://github.com/Soostone/string-conv")
    (synopsis "Standardized conversion between string types")
    (description
     "Avoids the need to remember many different functions for converting string
types.  Just use one universal function toS for all monomorphic string
conversions.")
    (license license:bsd-3)))

(define-public ghc-katip
  (package
    (name "ghc-katip")
    (version "0.8.7.4")
    (source (origin
              (method url-fetch)
              (uri (hackage-uri "katip" version))
              (sha256
               (base32
                "0gikcg4cya8gn7cs6n5i3a1xavzzn26y6hwnxng2s362bcscjqjv"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "katip")))
    (inputs (list ghc-aeson
                  ghc-async
                  ghc-auto-update
                  ghc-either
                  ghc-safe-exceptions
                  ghc-hostname
                  ghc-old-locale
                  ghc-string-conv
                  ghc-transformers-compat
                  ghc-unordered-containers
                  ghc-monad-control
                  ghc-transformers-base
                  ghc-resourcet
                  ghc-scientific
                  ghc-microlens
                  ghc-microlens-th
                  ghc-semigroups
                  ghc-unliftio-core))
    (native-inputs (list ghc-tasty
                         ghc-tasty-golden
                         ghc-tasty-hunit
                         ghc-tasty-quickcheck
                         ghc-quickcheck-instances
                         ghc-time-locale-compat
                         ghc-regex-tdfa))
    (home-page "https://github.com/Soostone/katip")
    (synopsis "A structured logging framework.")
    (description
     "Katip is a structured logging framework.  See README.md for more details.")
    (license license:bsd-3)))

(define-public ghc-cond
  (package
    (name "ghc-cond")
    (version "0.4.1.1")
    (source (origin
              (method url-fetch)
              (uri (hackage-uri "cond" version))
              (sha256
               (base32
		"12xcjxli1scd4asr4zc77i5q9qka2100gx97hv3vv12l7gj7d703"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "cond")))
    (home-page "https://github.com/kallisti-dev/cond")
    (synopsis "Basic conditional operators with monadic variants.")
    (description "")
    (license license:bsd-3)))

(define-public xwinwrap
  (let ((commit "539fc47e1dcb001bac7b2697924bb75e98bbb77c")
	(revision "20"))
    (package
      (name "xwinwrap")
      (version (git-version "0.0" revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://github.com/takase1121/xwinwrap")
	       (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
	  (base32 "1fgslxa7fqnm20fp6c5gvwkz5nihdp060784pdikdnkxrvykdyci"))))
      (build-system gnu-build-system)
      (arguments
       (list #:phases
	     #~(modify-phases %standard-phases
		 (add-after 'unpack 'fix-makefile
		   (lambda _
		     (mkdir-p (string-append #$output "/bin"))
		     (substitute* "Makefile" (("/usr/local") #$output))
		     #t))
		 (delete 'configure)
		 (delete 'check))))
      (inputs (list libx11 libxext libxrender))
      (home-page "https://github.com/takase1121/xwinwrap")
      (synopsis "")
      (description "Stick most of the apps to your desktop background")
      (license (license:non-copyleft #f "See xwinwrap.c.")))))

(define-public video-wall-dev
  (package
    (name "video-wall-dev")
    (version "1.1.0")
    (source (local-file "." "video-wall"
			#:select? (git-predicate ".")
			#:recursive? #t))
    (build-system haskell-build-system)
    (inputs (list ghc-lens
		  ghc-cond
		  ghc-katip
		  ghc-shelly
		  ghc-regex-tdfa
		  ghc-split
		  ghc-random-shuffle
		  ghc-xdg-basedir))
    (propagated-inputs (list xwinwrap xrandr xdotool procps mpv ffmpeg))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))

video-wall-dev
