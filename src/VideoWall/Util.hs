module VideoWall.Util where

import Control.Conditional
import Control.Lens.Operators
import Control.Monad.Cont (filterM, liftIO)
import Katip
import qualified Shelly as S
import System.Directory
import System.FilePath

-- | Monadic version of concatMap
concatMapM :: Monad m => (a -> m [b]) -> [a] -> m [b]
concatMapM f xs = fmap concat (mapM f xs)

-- | If @path@ is a file, returns that file. If @path@ is a directory gets all
-- files in the directory, and if @recuse@ is 'True' then gets all files in
-- subdirectories, recursively.
allFiles :: Bool -> FilePath -> IO [FilePath]
allFiles recurse path =
  condM
    [ (doesFileExist path, pure [path]),
      ( doesDirectoryExist path,
        if recurse
          then concatMapM (allFiles recurse . (path </>)) =<< listDirectory path
          else filterM doesFileExist . map (path </>) =<< listDirectory path
      ),
      (otherwiseM, pure [])
    ]

allFiles' :: Bool -> [FilePath] -> IO [FilePath]
allFiles' recurse =
  concatMapM
    ( \path ->
        condM
          [ (doesFileExist path, pure [path]),
            (doesDirectoryExist path, allFiles recurse path),
            (otherwiseM, pure [])
          ]
    )

-- | A form of 'ls' which is locked to 'String' as the input
ls' :: String -> LogStr
ls' = ls

-- | The 'shelly' function lifted.
liftSh a = liftIO $ S.shelly a
