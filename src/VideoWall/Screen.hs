{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module VideoWall.Screen where

import Control.Lens
import Control.Lens.TH
import Data.List
import qualified Data.Text as T
import Text.Read
import VideoWall.Util

default (T.Text)

type ScreenSpace = [Screen]

type Dimensions = (Int, Int)

type Position = (Int, Int)

type Screen = Rectangle

type Window = Rectangle

-- | A representation of the software side of a computer monitor, with resolution and its virtual position.
data Rectangle = Rectangle
  { _rectWidth :: Int,
    _rectHeight :: Int,
    _rectX :: Int,
    _rectY :: Int
  }
  deriving (Show, Read, Eq)

$(makeLenses ''Rectangle)

_rectPosition :: Rectangle -> Position
_rectPosition Rectangle {..} = (_rectX, _rectY)

rectDimensions :: Lens' Rectangle Position
rectDimensions f rect@Rectangle {..} =
  (\(w', h') -> rect {_rectWidth = w', _rectHeight = h'})
    <$> f (_rectWidth, _rectHeight)

_rectDimensions :: Rectangle -> Dimensions
_rectDimensions Rectangle {..} = (_rectWidth, _rectHeight)

rectPosition :: Lens' Rectangle Dimensions
rectPosition f rect@Rectangle {..} =
  (\(x', y') -> rect {_rectX = x', _rectY = y'}) <$> f (_rectX, _rectY)

-- | Parse a string in the format of "X+Y" into a Position. Nothing if the parsing failes.
parsePosition :: T.Text -> Maybe Position
parsePosition txt = case map T.unpack $ T.split (== '+') txt of
  [x, y] -> (,) <$> readMaybe x <*> readMaybe y
  _ -> Nothing

-- | Parse a string in the format of "WxH+X+Y" into a Screen. Nothing if the parsing fails.
parseScreen :: T.Text -> Maybe Screen
parseScreen txt = case map T.unpack $ T.split (`elem` ['x', '+']) txt of
  [w, h, x, y] ->
    Rectangle <$> readMaybe w <*> readMaybe h <*> readMaybe x <*> readMaybe y
  _ -> Nothing

-- | Find the Screen from the ScreenSpace the Position is in. Nothing if it is out of bounds.
findScreen :: Position -> ScreenSpace -> Maybe Screen
findScreen pt = find (inRectangle pt)

-- | A value indicating whether the given Position is within the bounds of the Screen.
inRectangle :: Position -> Rectangle -> Bool
inRectangle (x, y) Rectangle {..} =
  x
    >= _rectX
    && x
      <= _rectWidth
        + _rectX
    && y
      >= _rectY
    && y
      <= _rectHeight
        + _rectY

-- | Fit the first Dimensions into the bounds of the second while maintaining the aspect ratio.
fitTo :: Dimensions -> Dimensions -> Dimensions
fitTo (sw, sh) (mw, mh) =
  let ratio :: Double
      ratio =
        min
          (fromIntegral mw / fromIntegral sw)
          (fromIntegral mh / fromIntegral sh)
   in (sw, sh) & both %~ (round . (* ratio) . fromIntegral)

-- | Find the maximum ratio between widths and heights of each Dimensions.
ratioOf :: Dimensions -> Dimensions -> Double
ratioOf (w1, h1) (w2, h2) =
  max (fromIntegral w1 / fromIntegral w2) (fromIntegral h1 / fromIntegral h2)
