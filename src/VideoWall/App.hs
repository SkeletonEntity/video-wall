{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module VideoWall.App where

import Control.Conditional
import Control.Lens.Combinators
import Control.Lens.Operators
import Control.Lens.TH
import Control.Monad.Reader
import qualified Data.List.NonEmpty as NE
import qualified Data.Text as T
import Katip
import System.Directory
import System.Environment.XDG.BaseDir
import System.FilePath
import System.Random
import System.Random.Shuffle
import Text.Read
import VideoWall.Desktop
import VideoWall.Screen

default (T.Text)

data VideoBehavior
  = Loop
  | LoopRandom
  | SwitchEvery Int
  | SwitchEveryRandom Int
  deriving (Show, Read, Eq)

data FitType
  = Crop
  | Scale
  | Stretch
  | Set Dimensions
  deriving (Show, Read, Eq)

data Options = Options
  { _logSeverity :: Severity,
    _displayNumber :: Int,
    _xWinWrapArgs :: [String],
    _mpvArgs :: [String],
    _recursiveSearch :: Bool,
    _videoBehavior :: VideoBehavior,
    _fitType :: FitType,
    _geometry :: Maybe Screen,
    _forceSwitch :: Bool,
    _pickOne :: Bool,
    _save :: Bool
  }
  deriving (Show, Read, Eq)

$(makeLenses ''Options)

defaultOptions =
  Options
    { _logSeverity = ErrorS,
      _displayNumber = 0,
      _xWinWrapArgs = ["-ov", "-ni"],
      _mpvArgs =
        [ "--loop-playlist=inf",
          "--no-audio",
          "--no-input-default-bindings",
          "--no-osc",
          "--stop-screensaver=no",
          "--no-terminal"
        ],
      _recursiveSearch = False,
      _videoBehavior = Loop,
      _fitType = Crop,
      _geometry = Nothing,
      _forceSwitch = False,
      _pickOne = False,
      _save = True
    }

saveOptions :: (Options, [String]) -> IO ()
saveOptions (opts, files) = do
  saveDir <- (</>) <$> getHomeDirectory <*> getUserCacheDir "video-wall"
  createDirectoryIfMissing True saveDir
  files' <- mapM makeAbsolute files
  writeFile (saveDir </> "save") $ show (opts, files')

loadOptions :: IO (Maybe (Options, [String]))
loadOptions = do
  savePath <- (</>) <$> getHomeDirectory <*> getUserCacheFile "video-wall" "save"
  ifM (doesFileExist savePath) (readMaybe <$> readFile savePath) (pure Nothing)

data LogState = LogState
  { _logNameSpace :: Namespace,
    _logContext :: LogContexts,
    _logEnv :: LogEnv
  }

$(makeLenses ''LogState)

newtype Logger m a = Logger
  { unLogger :: ReaderT LogState m a
  }
  deriving (Functor, Applicative, Monad, MonadIO, MonadReader LogState)

$(makeLenses ''Logger)

instance (MonadIO m) => Katip (Logger m) where
  getLogEnv = view logEnv
  localLogEnv f (Logger m) = Logger (local (over logEnv f) m)

instance (MonadIO m) => KatipContext (Logger m) where
  getKatipContext = view logContext
  localKatipContext f (Logger m) = Logger (local (over logContext f) m)
  getKatipNamespace = view logNameSpace
  localKatipNamespace f (Logger m) = Logger (local (over logNameSpace f) m)

runLogger s f = runReaderT (unLogger f) s
