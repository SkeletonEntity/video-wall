{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module VideoWall.XWinWrap where

import Control.Concurrent
import Control.Conditional
import Control.Lens
import Control.Monad hiding (when)
import Control.Monad.Catch
import Control.Monad.Cont
  ( liftIO,
    void,
  )
import qualified Data.List.NonEmpty as NE
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import GHC.IO.Handle
import Katip
import Shelly (silently)
import qualified Shelly as S
import System.Directory
import System.Environment.XDG.BaseDir
import System.FilePath
import System.Process
import System.Random
import System.Random.Shuffle
import Text.Printf
import Text.Read
import Text.Regex.TDFA ((=~))
import VideoWall.App
import VideoWall.Desktop
import VideoWall.Screen
import VideoWall.Util

default (T.Text)

type PID = Int

data XWinWrap = XWinWrap
  { _xwScreen :: Screen,
    _xwArgs :: [String],
    _xwMPVArgs :: [String],
    _xwPaths :: NE.NonEmpty FilePath,
    _xwFitType :: FitType
  }

$(makeLenses ''XWinWrap)

-- | Shuffles the '_xwPaths' of a 'XWinWrap'.
randomizeVideoPaths :: XWinWrap -> IO XWinWrap
randomizeVideoPaths xwin = do
  newPaths <-
    NE.fromList
      . shuffle' (NE.toList $ xwin ^. xwPaths) (length $ xwin ^. xwPaths)
      <$> initStdGen
  pure (xwin & xwPaths .~ newPaths)

-- | Start an xwinwrap process using MPV as the replacer.
xWinWrap :: XWinWrap -> Logger IO (ProcessHandle, PID)
xWinWrap (XWinWrap screen args mpvArgs paths fit) = do
  $(logTM) InfoS $
    ls' $
      printf
        "screen dimensions are %s"
        (show (screen ^. rectDimensions))
  vDims <- liftSh $ S.silently $ getFileDimensions (NE.head paths)
  $(logTM) InfoS $ ls' $ printf "dimensions of the file are %s" (show vDims)
  $(logTM) InfoS $
    ls' $
      printf
        "when fitted the video dimensions become %s"
        (show $ (`fitTo` (screen ^. rectDimensions)) <$> vDims)
  let ratio@(x, y) =
        maybe (1, 1) (\v -> getScales fit v (screen ^. rectDimensions)) vDims
  $(logTM) InfoS $ ls' $ printf "scaling with the ratios %s" (show ratio)
  $(logTM) InfoS $ ls' $ printf "starting xWinWrap with args %s" (show $ processArgs x y)
  (_, _, _, ph) <- liftIO $ createProcess $ process x y
  pidm <- liftIO $ getPid ph
  when
    (isNothing pidm)
    (liftIO $ fail "Could not get the PID of created xwinwrap process")
  let pid = read $ show $ fromJust pidm
  $(logTM) InfoS $ ls' $ printf "started xwinwrap with PID of %d" pid
  pure (ph, pid)
  where
    processArgs scaleX scaleY =
      ["-g", showGeometry screen]
        <> args
        <> [ "--",
             "mpv",
             "--wid=%WID",
             "--video-scale-x=" <> show scaleX,
             "--video-scale-y=" <> show scaleY
           ]
        <> mpvArgs
        <> NE.toList paths
    process scaleX scaleY = proc "xwinwrap" (processArgs scaleX scaleY)

runLoop :: XWinWrap -> Logger IO ()
runLoop (XWinWrap screen args mpvArgs paths fit) = do
  killOldPID screen
  (ph, pid) <-
    xWinWrap $
      XWinWrap screen args mpvArgs paths (if length paths > 1 then Scale else fit)
  liftIO $ recordPID screen pid
  liftIO $ void $ waitForProcess ph

runLoopRandom :: XWinWrap -> Logger IO ()
runLoopRandom xwin = runLoop =<< liftIO (randomizeVideoPaths xwin)

runSwitchEvery :: Int -> Bool -> XWinWrap -> Logger IO ()
runSwitchEvery n force (XWinWrap screen args mpvArgs paths fit) = do
  killOldPID screen
  forever $
    forM_
      paths
      ( \file -> do
          $(logTM) InfoS $ ls' $ printf "starting xwinwrap process for %s" file
          videoMillis <-
            if force
              then pure Nothing
              else liftSh $ S.silently $ getFileMillis file
          (ph, pid) <- xWinWrap $ XWinWrap screen args mpvArgs (NE.singleton file) fit
          liftIO $ recordPID screen pid
          let sleepTime =
                maybe
                  n
                  ( \x -> x * max (round (fromIntegral n / fromIntegral x :: Double)) 1
                  )
                  videoMillis
          $(logTM) InfoS $
            ls' $
              printf
                "waiting %dms (%s loops) before switching"
                sleepTime
                (maybe "inf" (show . (sleepTime `div`)) videoMillis)
          liftIO $ threadDelay $ sleepTime * 1000
          pids <- liftSh $ S.silently getRunningPIDs
          oldPIDm <- liftIO $ getOldPID screen
          if pid `elem` pids && isJust oldPIDm && pid == fromJust oldPIDm
            then do
              $(logTM) InfoS $
                ls' $
                  printf
                    "killing xwinwrap process %d for %s"
                    pid
                    file
              killOldPID screen
            else
              liftIO $
                fail "previous xwinwrap process lost, ending switch process"
      )

runSwitchEveryRandom :: Int -> Bool -> XWinWrap -> Logger IO ()
runSwitchEveryRandom n force xwin =
  runSwitchEvery n force =<< liftIO (randomizeVideoPaths xwin)

recordPID :: Screen -> PID -> IO ()
recordPID screen pid = flip writeFile (show pid) =<< getPIDPath screen

killOldPID :: Screen -> Logger IO ()
killOldPID screen = do
  oldPIDm <- liftIO $ getOldPID screen
  case oldPIDm of
    Nothing ->
      $(logTM) InfoS "No old PID found, assuming this is the first launch"
    Just oldPID -> do
      $(logTM) InfoS $ ls' $ printf "found old PID %d" oldPID
      pids <- liftSh $ S.silently getRunningPIDs
      if null pids
        then do
          $(logTM) InfoS $ ls "no instances of xwinwrap to kill"
          cleanOldPIDs
        else do
          $(logTM) InfoS $
            ls' $
              printf
                "found xwinwrap process(es) with PID(s) %s"
                (show pids)
          if oldPID `notElem` pids
            then
              $(logTM) InfoS $
                ls' $
                  printf
                    "none of the xwinwrap instances have a PID of %d"
                    oldPID
            else do
              $(logTM) InfoS $
                ls' $
                  printf
                    "attempting to kill old PID %s"
                    (show oldPID)
              liftSh $ S.errExit False $ S.run "kill" [T.pack $ show oldPID]
              pure ()

cleanOldPIDs :: Logger IO ()
cleanOldPIDs = do
  dir <- liftIO $ (</>) <$> getHomeDirectory <*> pure ".local/var/video-wall"
  liftIO $ createDirectoryIfMissing True dir
  files <- liftIO $ filter ((=~ "[0-9x+]+") . takeFileName) <$> listDirectory dir
  $(logTM) InfoS $ ls' $ printf "deleting old PID files: %s" (show files)
  liftIO $ mapM_ (removeFile . (dir </>)) files

getOldPID :: Screen -> IO (Maybe Int)
getOldPID screen = do
  pidPath <- getPIDPath screen
  ifM (doesFileExist pidPath) (readMaybe <$> readFile pidPath) (pure Nothing)

getPIDPath :: Screen -> IO FilePath
getPIDPath screen = do
  dir <- (</>) <$> getHomeDirectory <*> getUserCacheDir "video-wall"
  createDirectoryIfMissing True dir
  pure $ dir </> showGeometry screen

getRunningPIDs :: S.Sh [PID]
getRunningPIDs =
  catch
    (map read . words . T.unpack <$> S.run "pidof" ["xwinwrap"])
    ((\_ -> pure []) :: SomeException -> S.Sh [Int])

getFileMillis :: FilePath -> S.Sh (Maybe Int)
getFileMillis path =
  fmap (round . (\i -> i * 1000 :: Double)) . readMaybe . T.unpack
    <$> S.run
      "ffprobe"
      [ "-i",
        T.pack path,
        "-v",
        "quiet",
        "-show_entries",
        "format=duration",
        "-hide_banner",
        "-of",
        "default=noprint_wrappers=1:nokey=1"
      ]

getScales :: FitType -> Dimensions -> Dimensions -> (Double, Double)
getScales fit vDims dDims@(dw, dh) =
  let vDims'@(vw, vh) = vDims `fitTo` dDims
      def = (1, 1)
   in case fit of
        Crop -> def & both .~ dDims `ratioOf` vDims'
        Scale -> def
        Stretch ->
          (fromIntegral dw / fromIntegral vw, fromIntegral dh / fromIntegral vh)
        Set (w, h) ->
          (fromIntegral w / fromIntegral vw, fromIntegral h / fromIntegral vh)

getFileDimensions :: FilePath -> S.Sh (Maybe Dimensions)
getFileDimensions file = do
  elems <-
    T.splitOn ","
      <$> S.run
        "ffprobe"
        [ "-v",
          "error",
          "-select_streams",
          "v:0",
          "-show_entries",
          "stream=width,height",
          "-of",
          "csv=p=0",
          T.pack file
        ]
  case elems of
    [w, h] -> pure $ sequenceOf both $ (w, h) & both %~ (readMaybe . T.unpack)
    _ -> pure Nothing
