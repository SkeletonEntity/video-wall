{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module VideoWall.Desktop where

import Control.Conditional
import Data.Maybe
import qualified Data.Text as T
import Shelly hiding (when)
import Text.Printf
import VideoWall.Screen

default (T.Text)

-- | Returns the Screen based on the display number. The number is based on the
-- order screens appear in xrandr, but the primary display is always 0.
getScreen :: Int -> Sh Screen
getScreen d = do
  space <- getScreenSpace
  when
    (length space <= d)
    ( fail $
        printf
          "%d display(s) found, but display %d specified (the list is 0-indexed)."
          (length space)
          d
    )
  pure $ space !! d

-- | Get the ScreenSpace of the OS.
getScreenSpace :: Sh ScreenSpace
getScreenSpace = do
  primary <-
    escaping False $
      run "xrandr" ["|", "awk", "'/primary/ {print $4}'"]
  rest <-
    filter (/= "primary") . T.lines
      <$> escaping
        False
        (run "xrandr" ["|", "awk", "'/ connected / {print $3}'"])
  pure $ mapMaybe parseScreen (primary : rest)

showGeometry :: Screen -> String
showGeometry Rectangle {..} =
  show _rectWidth
    <> "x"
    <> show _rectHeight
    <> "+"
    <> show _rectX
    <> "+"
    <> show _rectY
